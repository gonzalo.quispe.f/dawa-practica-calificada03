// var socket = io();
var socket = io({ transports: ["websocket"], upgrade: false });

var params = new URLSearchParams(window.location.search);

if (!params.has("nombre")) {
    // window.location = "index.html";
    window.location = "index.html";
    throw new Error("El nombre es necesario");
}

var usuario = {
    nombre: params.get("nombre"),
};

var users_list = document.getElementById("users_list");

socket.on("connect", function () {
    console.log("Conectado al servidor");
    socket.emit("entrarChat", usuario, function (resp) {
        // mostrar usuario actual
        $("#usuario_actual").text(params.get("nombre"));

        // mostrar lista de usuarios conectados:
        users_list.innerHTML = "";
        for (let i = 0; i <= resp.length; i++) {
            users_list.insertAdjacentHTML(
                "beforeend",
                `
                <li class="active">
                    <div class="d-flex bd-highlight">
                        <div class="img_cont">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/User_with_smile.svg/1200px-User_with_smile.svg.png" class="rounded-circle user_img">
                            <span class="online_icon"></span>
                        </div>
                        <div class="user_info">
                            <span>${resp[i].nombre}</span>
                            <p>Online</p>
                        </div>
                    </div>
                </li>
                `
            );
            // var li = document.createElement("li");
            // li.appendChild(document.createTextNode(resp[i].nombre));
            // users_list.appendChild(li);
        }
    });
});

socket.on("disconnect", function () {
    console.log("Perdimos conexión con el servidor");
});

socket.on("crearMensaje", function (mensaje) {
    console.log("Servidor mensaje:", mensaje);
    var elem = document.getElementById("parrafo");
    elem.innerHTML = "que pasa papu";
    // console.log(listaUsuarios)
});

// Escuchar cambios de usuarios
// cuando un usuario entra o sale del chat

socket.on("listaPersonas", function (personas) {
    console.log(personas);
    // mostrar lista de usuarios conectados:
    users_list.innerHTML = "";
    for (let i = 0; i <= personas.length; i++) {
        users_list.insertAdjacentHTML(
            "beforeend",
            `
            <li class="active">
                <div class="d-flex bd-highlight">
                    <div class="img_cont">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/User_with_smile.svg/1200px-User_with_smile.svg.png" class="rounded-circle user_img">
                        <span class="online_icon"></span>
                    </div>
                    <div class="user_info">
                        <span>${personas[i].nombre}</span>
                        <p>Online</p>
                    </div>
                </div>
            </li>
            `
        );
        // var li = document.createElement("li");
        // li.appendChild(document.createTextNode(resp[i].nombre));
        // users_list.appendChild(li);
    }
});
