const express = require("express");
const http = require("http");

// TODO: REPASSAR QUE ES LO QUE HACE EXACTAMENTE ESTA LIBRERÍA
const path = require("path");

const { Usuarios } = require("./classes/usuarios");
const usuarios = new Usuarios();

const { crearMensaje } = require("./utilidades/utilidades");

const app = express();

const publicPath = path.resolve(__dirname, "../public");
const port = process.env.PORT || 3000;
app.use(express.static(publicPath));

let server = http.createServer(app);

const socketIO = require("socket.io");
let io = socketIO(server); // IO = esta es la comunicación del backend
io.setMaxListeners(2);

io.on("connection", (client) => {
    console.log("usuario conectado");

    usuarioSeConecto(client);

    crearMensajeDesdeServer(client);

    desconectarUsuario(client);

    crearMensajeConJquery(client);
});

server.listen(port, (err) => {
    if (err) throw new Error(err);
    console.log(`Servidor corriendo en puerto ${port}`);
});






// FUNCIONES:__________________________________

function enviarDesdeServer(client) {
    client.emit("enviarMensaje", {
        usuario: "Gonzalo el Administrador",
        mensaje: "Bienvenido a esta aplicación",
    });
}

function usuarioSeDesconecto(client) {
    client.on("disconnect", () => {
        console.log("Usuario desconectado");
    });
}

function recibirDesdeCliente(client) {
    // Escuchar al cliente:
    client.on("enviarMensaje", (mensaje) => {
        console.log(mensaje);
    });
}

function enviarBroadCast(client) {
    client.on("enviarMensaje", (data, callback) => {
        console.log(data);
        client.broadcast.emit("enviarMensaje", data);
    });
}

// FUNCIONES LAB 13 Y EXAMEN3:

function usuarioSeConecto(client) {
    let lista_personas = usuarios.getPersonas();
    const cliente_ya_conectado = lista_personas.find(
        (element) => element.name == client.name
    );
    console.log("cliente ya conectado", cliente_ya_conectado);
    // if (cliente_ya_conectado === undefined) {
    client.on("entrarChat", (data, callback) => {
        if (!data.nombre) {
            return callback({
                error: true,
                mensaje: "El nombre es necesario",
            });
        }
        let personas = usuarios.agregarPersonas(client.id, data.nombre);

        client.broadcast.emit("listaPersonas", usuarios.getPersonas());
        callback(personas);
    });
    // }
    // conexiones socket creadas:
    var room = io.sockets.adapter.rooms;
    // console.log("conexiones: ", Object.keys(room).length);
    // console.log("conexiones: ", room);
}

function crearMensajeDesdeServer(client) {
    client.on("crearMensaje", (data) => {
        let persona = usuarios.getPersona(client.id);
        let mensaje = crearMensaje(persona.nombre, data.mensaje);
        client.broadcast.emit("crearMensaje", mensaje);
    });
}

function desconectarUsuario(client) {
    client.on("disconnect", () => {
        let personaBorrada = usuarios.borrarPersona(client.id);

        if (personaBorrada != false) {
            client.broadcast.emit("crearMensaje", {
                usuario: "Administrador",
                mensaje: crearMensaje(
                    "Admin",
                    `${personaBorrada.nombre} salio`
                ),
                listaUsuarios: usuarios.getPersonas(),
            });
        }

        client.broadcast.emit("listaPersonas", usuarios.getPersonas());
    });
}

function crearMensajeConJquery(client) {
    client.on("chat message", (msg, usuario_que_hablo) => {
        io.emit("mensaje desde server", msg, usuario_que_hablo);
    });
}
